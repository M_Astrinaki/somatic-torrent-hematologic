#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: `basename $0` <run-id>"
    exit 1
fi

ID=$1
## http://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

ID=$1
SERVER=${CROMWELL_SERVER:-http://cromwell.testbed-precmed.iit.demokritos.gr}
>&2 echo "${red}Using Cromwell at $SERVER..${reset}"
curl -s "$SERVER/api/workflows/v1/$1/metadata" | jq -r '.failures[]|.causedBy[]|.message'
